#include "Helper.h"

Helper::Helper() {
}

char* Helper::StrToCharPtr(string str) {
	char* c = new char[str.length() + 1];
	std::strcpy(c,str.c_str());
	return c;
}

string Helper::CharPtrToStr(char* c) {
	string str(c);
	return str;
}

string Helper::IntToStr(int i) {
	string str;
	ostringstream convert;
	convert << i;
	str = convert.str();
	return str;
}

int Helper::StrToInt(string s) {
	return atoi(s.c_str());
}

int Helper::CharPtrToInt(char* c) {
	return atoi(c);
}

void Helper::SplitString(char* s, char* delims, vector<string>& out) {
	char* str = strtok(s,delims);
	while (str != NULL)
	{
		out.push_back(str);
		str = strtok (NULL, delims);
	}
}

void Helper::SplitString(string str, char* delims, vector<string>& out) {
	SplitString(StrToCharPtr(str), delims, out);
}

	
