#ifndef HELPER_H
#define HELPER_H

#include <stdlib.h>
#include <string>
#include <sstream>
#include <iostream>
#include <cstring>
#include <vector>

using namespace std;

class Helper {
public:
	Helper();
	char* StrToCharPtr(string str);
	string CharPtrToStr(char* c);
	string IntToStr(int i);
	int StrToInt(string s);
	int CharPtrToInt(char* c);
	void SplitString(char* s, char* delims, vector<string>& out);
	void SplitString(string str, char* delims, vector<string>& out);	
private:
};


#endif
