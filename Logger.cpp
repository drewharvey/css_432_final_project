#include "Logger.h"

Logger::Logger() {
	this->logFilePath = DEFAULT_PATH;
}

Logger::Logger(string logFilePath) {
	this->logFilePath = logFilePath;
}

void Logger::Log(string message) {

	ofstream logFile;		// our logfile
	time_t rawTime;			// raw time in time_t format
	struct tm* localTime;	// holds equivilent local time format

	// get current time
	time(&rawTime);
	localTime = localtime(&rawTime);

	// open log file
	logFile.open(helper.StrToCharPtr(logFilePath), ios::out | ios::app);

	// write current timestamp in log
	logFile << asctime(localTime) << ": ";

	// write our message into the file
	logFile << message << "\n";

	// save and close file
	logFile.close();
}



