#ifndef LOGGER_H
#define LOGGER_H

#include <string.h>
#include <fstream>
#include <time.h>
#include "Helper.h"

using namespace std;

#define DEFAULT_PATH "./log"

class Logger {

public:
	Logger();
	Logger(string logFilePath);
	void Log(string message);

private:
	string logFilePath;
	Helper helper;
};

#endif


