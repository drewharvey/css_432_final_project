#include "MySocket.h"

#define DEBUG false

MySocket::MySocket() {
	readBuffer = (char*) malloc(BUFFER_SIZE);
}

MySocket::MySocket(int sd) {
	socketDescriptor = sd;
}

void MySocket::CreateSocket(int port) {
	if (DEBUG) cout << "MySocket::CreateSocket() Creating Socket...";
	bzero( (char*)&SockAddr, sizeof( SockAddr ) );		// zero out values
    SockAddr.sin_family      = AF_INET; 				// Address Family Internet
	SockAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	SockAddr.sin_port = htons(port);
	socketDescriptor = socket( AF_INET, SOCK_STREAM, 0 );
	if (DEBUG) cout << "Done" << endl;
}

void MySocket::CreateSocket(char* hostName, int hostPort) {
	if (DEBUG) cout << "MySocket::CreateSocket(" << hostName << ", " << hostPort << ") Creating Socket...";

	// get host details by supplying host name
	struct hostent* host = gethostbyname( hostName );	
	
    bzero( (char*)&SockAddr, sizeof( SockAddr ) );		// zero out values
	
    SockAddr.sin_family      = AF_INET; 				// Address Family Internet
    SockAddr.sin_addr.s_addr =
      inet_addr( inet_ntoa( *(struct in_addr*)*host->h_addr_list ) ); 
    SockAddr.sin_port        = htons( hostPort );		// set server's port
	
	// Open a stream-oriented socket with the Internet address family
	socketDescriptor = socket( AF_INET, SOCK_STREAM, 0 );
	
	if (DEBUG) cout << "Done" << endl;
}

int MySocket::Connect() {
	if (DEBUG) cout << "MySocket::Connect()" << endl;
	return connect( socketDescriptor, ( sockaddr* )&SockAddr, sizeof( SockAddr ) );
}

int MySocket::Close() {
	if (DEBUG) cout << "MySocket::Close()" << endl;
	return close(socketDescriptor);
}
int MySocket::Close(int& sd) {
	return close(sd);
}

void MySocket::StartListening() {
	if (DEBUG) cout << "MySocket::WaitForConnection()" << endl;

	bind(socketDescriptor, (sockaddr*) & SockAddr, sizeof (SockAddr));
    listen(socketDescriptor, 100); // start listening to port
}

void MySocket::WaitForConnection(int& sd) {
	sockaddr_in newSockAddr; // sockaddr_in for accepting connection
	socklen_t newSockAddrSize = sizeof (newSockAddr);
	
	// store connection in sd
	sd = accept(socketDescriptor, (sockaddr *) & newSockAddr, &newSockAddrSize); 
}

char* MySocket::Read() {
	return Read(socketDescriptor);
}
char* MySocket::Read(int& sd) {
	if (DEBUG) cout << "MySocket::Read()" << endl;

	bzero(readBuffer, BUFFER_SIZE);
	int byte_count = recv(sd, readBuffer, BUFFER_SIZE, 0);

	if (byte_count >= 0) {
		readBuffer[byte_count] = '\0'; // add null character at the end
	}
	return readBuffer;
}

char* MySocket::ReadFile(int& fileSize) {
	if (DEBUG) cout << "MySocket::ReadFile()" << endl;

	bzero(readBuffer, BUFFER_SIZE);
	fileSize = recv(socketDescriptor, readBuffer, BUFFER_SIZE, 0);

	return readBuffer;
}


int MySocket::Send(string data) {
    return Send(socketDescriptor, data);
}
int MySocket::Send(int& sd, string data) {
	if (DEBUG) cout << "MySocket::Send(): " << data << endl;
    return send(sd, data.c_str(), strlen(data.c_str()), 0);
}

int MySocket::SendFile(char* file, int fileSize) {
	return SendFile(socketDescriptor, file, fileSize);
}
int MySocket::SendFile(int& sd, char* file, int fileSize) {
	return send(sd, file, fileSize, 0);
}




