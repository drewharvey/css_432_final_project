#ifndef MYSOCKET_H
#define MYSOCKET_H

#include <sys/types.h>    // socket, bind
#include <sys/socket.h>   // socket, bind, listen, inet_ntoa
#include <netinet/in.h>   // htonl, htons, inet_ntoa
#include <arpa/inet.h>    // inet_ntoa
#include <netdb.h>        // gethostbyname
#include <unistd.h>       // read, write, close
#include <string.h>       // bzero
#include <netinet/tcp.h>  // SO_REUSEADDR
#include <sys/uio.h>      // writev
#include <iostream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define BUFFER_SIZE 5120 	// 5MB max file/message size

using namespace std;

class MySocket {

public:
	MySocket();
	MySocket(int sd);				// use socket desc to load mysocket
	void CreateSocket(int);			// socket using any addr
	void CreateSocket(char*, int);	// socket using specific addr
	int Connect();
	int Close();
	int Close(int&);
	void StartListening();
	void WaitForConnection(int&);
	char* Read();
	char* Read(int&);
	char* ReadFile(int&);
	int Send(string);
	int Send(int&, string);
	int SendFile(char*, int size);
	int SendFile(int&, char*, int size);
	
private:	
	sockaddr_in SockAddr;
	int socketDescriptor;		// socket descriptor
	int connectedSd;			// socket desc for accpeted connection
	char* readBuffer;
};

#endif
