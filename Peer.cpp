#include "Peer.h"

Peer::Peer() {
}

Peer::Peer(std::string ipAddress, int port) {
	this->ipAddress = ipAddress;
	this->port = port;
}

string Peer::GetIpAddress() {
	return ipAddress;
}

char* Peer::GetIpAddressChar() {
	return (char*)ipAddress.c_str();
}

void Peer::SetIpAddress(string ipAddress) {
	this->ipAddress = ipAddress;
}

void Peer::SetIpAddress(char* ipAddress) {
	this->ipAddress = helper.CharPtrToStr(ipAddress);
}

int Peer::GetPort() {
	return port;
}

void Peer::SetPort(int port) {
	this->port = port;
}

void Peer::SetPort(std::string port) {
	this->port = helper.StrToInt(port);
}

void Peer::SetPort(char* port) {
	this->port = helper.CharPtrToInt(port);
}


