#ifndef PEER_H
#define PEER_H

#include <string>      		
#include <sstream>
#include <iostream>
#include <cstring>
#include <vector>
#include "Helper.h"

using namespace std;

class Peer {
public:
	Peer();
	Peer(string ipAddress, int port);
	
	// Ip Address Getters and Setters 
	string GetIpAddress();
	char* GetIpAddressChar();
	void SetIpAddress(string ipAddress);
	void SetIpAddress(char* ipAddress);
	
	// Port Getters and Setters
	int GetPort();
	void SetPort(int port);
	void SetPort(string port);
	void SetPort(char* port);
	
private:
	Helper helper;
	string ipAddress;
	int port;
};

#endif
