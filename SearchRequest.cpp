#include "SearchRequest.h"

SearchRequest::SearchRequest() {
}
SearchRequest::SearchRequest(int ID) {
	this->ID = ID;
}
SearchRequest::SearchRequest(int ID, string ip, int port) {
	this->ID = ID;
	this->ipAddress = ip;
	this->port = port;
}

int SearchRequest::GetID() {
	return ID;
}

string SearchRequest::GetIP() {
	return ipAddress;
}

int SearchRequest::GetPort() {
	return port;
}

