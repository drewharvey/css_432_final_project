#ifndef SEARCHREQUEST_H
#define SEARCHREQUEST_H

#include <string>      		
#include <sstream>
#include <iostream>
#include <cstring>

using namespace std;

class SearchRequest {
public:
	SearchRequest();
	SearchRequest(int);
	SearchRequest(int, string, int);
	int GetID();
	string GetIP();
	int GetPort();
private:
	int ID;
	string ipAddress;
	int port;
};

#endif



