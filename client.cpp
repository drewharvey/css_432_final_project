#include <sys/types.h>    // socket, bind
#include <sys/socket.h>   // socket, bind, listen, inet_ntoa
#include <netinet/in.h>   // htonl, htons, inet_ntoa
#include <arpa/inet.h>    // inet_ntoa
#include <netdb.h>        // gethostbyname
#include <unistd.h>       // read, write, close
#include <string>      		// bzero
#include <sstream>
#include <netinet/tcp.h>  // SO_REUSEADDR
#include <sys/uio.h>      // writev
#include <iostream>
#include <signal.h>
#include <fcntl.h>
#include <sys/time.h>
#include <cstring>
#include <vector>
#include <dirent.h>
#include <pthread.h>
#include <fstream>

#include "MySocket.h"
#include "Peer.h"
#include "SearchRequest.h"
#include "Helper.h"
#include "Logger.h"

#define DEBUG true

using namespace std;

class Client {
	public:
		Client(int port, char* masterServAddr, int masterServPort) {
			masterServerHostname = masterServAddr;
			masterServerPort = masterServPort;
			myPort = port;
			StoreMyAddress();
		}
		
		~Client() {
		
		}

		void SetPartnerPort(int port) {
			this->partnerPort = port;
		}
		
		//----------------------------------------------------
		// MASTER SERVER METHODS
		//----------------------------------------------------

		void RegisterWithServer() {
			logger.Log("Registering with master server");
			ConnectTo(masterServerHostname, masterServerPort);
			mySocket.Send("0:" + helper.IntToStr(myPort));
			mySocket.Close();
			cout << "Your are connected at: " << myIpAddress << ":" << myPort << endl;
		}
		
		void RequestPeerList() {
			logger.Log("Requesting peer list from master server");

			// clear previous peer list
			peerList.clear();

			// connect
			ConnectTo(masterServerHostname, masterServerPort);

			// send request
			mySocket.Send("1:5");

			// read reply (list of peers)
			char* list = mySocket.Read();
			vector<string> peerListStr;
			helper.SplitString(list, "\n", peerListStr);

			// load peers into our list
			vector<string> peerInfo;
			for (int i = 0; i < peerListStr.size(); i++) {
			
				peerInfo.clear();
			
				// get ip and port for peer
				helper.SplitString(peerListStr[i], ",", peerInfo);
				
				// add new peer to list
				peerList.push_back(Peer());
				peerList.back().SetIpAddress(peerInfo[0]);
				peerList.back().SetPort(peerInfo[1]);
			}
			
			mySocket.Close();
		}

		void UnregisterFromServer() {
			logger.Log("Unregistering from master server");
			ConnectTo(masterServerHostname, masterServerPort);
			mySocket.Send("2:" + helper.IntToStr(masterServerPort));
			mySocket.Close();
		} 
		
		//----------------------------------------------------
		// PASSIVE REQUEST HANDLER
		//----------------------------------------------------

		void ListenForRequests() {
			logger.Log("Starting listening for requests");

			int sd;							// connection socket desc
			char* msg;						// message received
			int command;					// command #
			vector<string> reply;			// split reply into pieces

			mySocket.CreateSocket(myPort);	// setup server socket
			mySocket.StartListening();		// bind and listen on socket

			while (true) {

				// capture connection in new socket
				mySocket.WaitForConnection(sd);	

				// read request 
				msg = mySocket.Read(sd);			
				helper.SplitString(msg, ":", reply);

				// get command #
				command = helper.StrToInt(reply[0]);	
				
				// do something based on the message's command number
				switch (command) {
					case 4:	// Request a file // 4:filename
						ReplyToFileRequest(reply[1], sd);
						break;
					case 5:	// List all files // 5: 
						ReplyToFileListRequest(sd);
						break;
					case 6:	// Search for file // 6:ID:File String:RequestingIP:RequestingPort:TTL
						ReplyToSearchRequest(helper.StrToInt(reply[1]), reply[2], reply[3], helper.StrToInt(reply[4]), sd);
						break;
					default:
						break;
				}

				reply.clear();
				mySocket.Close(sd);	
			}
				
		}

		//----------------------------------------------------
		// SEND REQUEST METHODS
		//----------------------------------------------------
		
		void RequestFile(string filename) {
			logger.Log("Requesting file download");

			RequestPeerList();	// update peers
			vector<string> files;
			bool fileFound = false;
			int peerWithFile = -1;

			for (int i = 0 ; i < peerList.size() && !fileFound; i++) {

				GetPeerFiles(peerList[i], files);

				// search for filename in this set of files
				for (int j = 0; j < files.size() && !fileFound; j++) {	
					if (filename.compare(files[j]) == 0) {
						fileFound = true;
						peerWithFile = i;
					}
				}

				files.clear();
			}

			// tell user if file does not exists on network
			if (!fileFound) {
				cout << "File could not be found on network" << endl;
				logger.Log("Failed to find file on network");
				return;
			}

			// show who we are trying to download from
			cout << "File Found! Requesting download from: ";
			cout << peerList[peerWithFile].GetIpAddress() << ":" << peerList[peerWithFile].GetPort() << endl;

			// send a request to that peer
			ConnectToPeer(peerList[peerWithFile]);
			mySocket.Send("4:" + filename);

			// setup vars for reply
			char* reply;
			int fileSize;

			reply = mySocket.ReadFile(fileSize);

			// create new file and open in binary mode
			ofstream newFile(helper.StrToCharPtr(filename), ios::out | ios::binary);

			// write our reply into the file
			newFile.write(reply, fileSize);

			// save and close file
			newFile.close();

			cout << filename << " successfully transfered!" << endl;
			logger.Log("File successfully downloaded");
		}

		
		void SearchForFile(string filename) {
			logger.Log("Requesting file search");
			RequestPeerList();
			cout << "Search results for: \"" << filename << "\"" << endl;

			// send request to each peer
			for (int i = 0; i < peerList.size(); i++) {
				SendFileSearchRequest(filename, peerList[i]);
			}
		}

		void ListPeerFiles() {
			logger.Log("Requesting list of peer's files");

			RequestPeerList();

			// get files and print for each peer
			vector<string> files;
			for (int i = 0; i < peerList.size(); i++) {
				GetPeerFiles(peerList[i], files);
				PrintPeerFiles(files);
				files.clear();
			}
		}

		void PrintPeers() {
			RequestPeerList();

			// print details of each peer
			for (int i = 0; i < peerList.size(); i++) {
				cout << "Peer " << i << ": " << peerList[i].GetIpAddress() << ":" << peerList[i].GetPort() << endl;
			}
		}
		
	private:
	
		//----------------------------------------------------
		// CLASS MEMBERS
		//----------------------------------------------------
		
		MySocket mySocket;				// socket to use

		char* masterServerHostname;		// hostname of bootstrap server
		int masterServerPort;			// port of boostrap server

		char* myIpAddress;				// the ip of this machine
		int myPort;						// the port of this application
		int partnerPort;				// passive and active use different ports
		
		vector<Peer> peerList;			// holds all peer ip's and ports
		vector<SearchRequest> requestHistory;		// holds list of unique search requests
		int myLastRequestID;			// the last request we sent 
		
		Helper helper;					// string, char, and int functions
		Logger logger;					// used to log actions
		
		//----------------------------------------------------
		// PEER-2-PEER PRIVATE FUNCTIONS
		//----------------------------------------------------

		bool ConnectTo(char* hostname, int port) {
			mySocket.CreateSocket(hostname, port);	// create our socket
			if (mySocket.Connect() < 0) {
				cout << "Error connecting to " << hostname << ":" << port << endl;
			}
		}
		bool ConnectTo(string hostname, int port) {
			return ConnectTo(helper.StrToCharPtr(hostname), port);
		}
		
		bool ConnectToPeer(Peer peer) {
			return (ConnectTo(peer.GetIpAddress(), peer.GetPort()) <= 0);
		}

		void SendToAllPeers(string message) {

			for (int i = 0; i < peerList.size(); i++) {

				// send if its not me and we connect successfully
				if (!IsMe(peerList[i].GetIpAddress(), peerList[i].GetPort())
					&& ConnectToPeer(peerList[i])) 
				{
					mySocket.Send(message);
					mySocket.Close();
				}
			}
		}
		
		void GetPeerFiles(Peer peer, vector<string>& out) {
			if (IsMe(peer.GetIpAddress(), peer.GetPort()))
				return;
				
			if (!ConnectToPeer(peer))
				return;

			logger.Log("Requesting file list");
			
			char* reply;
			mySocket.Send("5:");
			reply = mySocket.Read();
			helper.SplitString(reply, "\n", out);
			mySocket.Close();
		}

		void PrintPeerFiles(vector<string>& files) {
			for (int i = 0; i < files.size(); i++) {
				cout << files[i] << endl;
			}
		}
		
		void SendFileSearchRequest(string filename, Peer peer) {
			// don't connect to ourselves
			if (IsMe(peer.GetIpAddress(), peer.GetPort()))
				return;

			if (!ConnectToPeer(peer))
				return;

			logger.Log("Sending search request for file \"" + filename + "\"");
			
			string request = "";			
			char* reply;
			myLastRequestID++;		// increment for a unique request id
			
			// push new search request into our history with our details
			requestHistory.push_back(SearchRequest(myLastRequestID, helper.CharPtrToStr(myIpAddress), myPort));
			
			// build request message
			request.append("6");
			request.append(":");
			request.append(helper.IntToStr(requestHistory.back().GetID()));
			request.append(":");
			request.append(filename);
			request.append(":");
			request.append(helper.CharPtrToStr(myIpAddress));
			request.append(":");
			request.append(helper.IntToStr(myPort));
			request.append(":10");
			request.append("\0");

			mySocket.Send(request);	

			reply = mySocket.Read();

			vector<string> replyParts;					
			helper.SplitString(reply, ":", replyParts);	

			// if the reply is type 7, its a confirmation that file exists
			if (replyParts.size() > 0 && helper.StrToInt(replyParts[0]) == 7) {
				cout << peer.GetIpAddress() << ":" << peer.GetPort() << " has the file!" << endl;
				logger.Log("Peer has file");
			}

			replyParts.clear();
			mySocket.Close();
		}
		
		bool SearchRequestExists(SearchRequest sr) {
			for (int i = 0; i < requestHistory.size(); i++) {
				if (requestHistory[i].GetID() == sr.GetID()
					&& requestHistory[i].GetIP() == sr.GetIP()
					&& requestHistory[i].GetPort() == sr.GetPort()) 
				{
					return true;
				}
			}
			return false;
		}


		//----------------------------------------------------
		// REPLY TO REQUEST METHODS
		//----------------------------------------------------

		void ReplyToFileRequest(string filename, int& sd) {
			if (!FileExists(filename)) {
				mySocket.Send(sd, "\0");
				return;
			}

			logger.Log("Sending reply to search request");

			ifstream file;
			char* fileBuffer;
			int size; 

			//open file in binary mode
			file.open(helper.StrToCharPtr(filename), ios::binary);	
			
			if (file.is_open()) {

				// get file size
				file.seekg(0, ios::end);		
				size = file.tellg();
				file.seekg(0, ios::beg);

				// read file into buffer
				fileBuffer = new char[size];
				file.read(fileBuffer, size);

				// send file
				mySocket.SendFile(sd, fileBuffer, size);

				file.close();
				delete fileBuffer;
			}	
		}

		void ReplyToFileListRequest(int& sd) {
			logger.Log("Sending reply to file list request");
			string message = GetMyFiles();
			mySocket.Send(sd, message);
		}
		
		void ReplyToSearchRequest(int requestID, string filename, string ip, int port, int& sd) {
			logger.Log("Sending reply to search request");

			// create request with supplied info
			SearchRequest sr(requestID, ip, port);

			// check if we've already dealt with this request
			if (SearchRequestExists(sr)) {
				return;
			}

			// add to request history
			requestHistory.push_back(sr);


			if (FileExists(filename)) {
				// send response saying we have file
				string reply = "7";
				reply.append(":");
				reply.append(helper.IntToStr(requestID));
				reply.append(":");
				reply.append(filename);
				reply.append(":");
				reply.append(myIpAddress);
				reply.append(":");
				reply.append(helper.IntToStr(myPort));
				mySocket.Send(sd, reply);
			}
			else {	
				// forward request to peers
				string searchRequest = "6";
				searchRequest.append(":");
				searchRequest.append(helper.IntToStr(requestID));
				searchRequest.append(":");
				searchRequest.append(filename);
				searchRequest.append(":");
				searchRequest.append(ip);
				searchRequest.append(":");
				searchRequest.append(helper.IntToStr(port));
				SendToAllPeers(searchRequest);
			}
		}
		
		//----------------------------------------------------
		// HELPER FUNCTIONS
		//----------------------------------------------------
		
		string GetMyIpAddress() {
			char ac[80];
			gethostname(ac, sizeof(ac));
			struct hostent *phe = gethostbyname(ac);			
			struct in_addr addr;
			memcpy(&addr, phe->h_addr_list[0], sizeof(struct in_addr));
			return inet_ntoa(addr);
		}

		void StoreMyAddress() {
			myIpAddress = helper.StrToCharPtr(GetMyIpAddress());
		}

		bool IsMe(string ip, int port) {
			string myIp = helper.CharPtrToStr(myIpAddress);
			return (myIp.compare(ip) == 0) 
				&& (myPort == port || partnerPort == port);
		}
		
		bool FileExists(string filename) {
			vector<string> files;
			GetFilesInDir(".", files);
			for (int i = 0; i < files.size(); i++) {
				if (filename.compare(files[i]) == 0) {
					files.clear();
					return true;
				}
			}
			files.clear();
			return false;
		}

		string GetMyFiles() {
			string filesStr = "";
			vector<string> files;
			GetFilesInDir(".", files);
			for (int i = 0; i < files.size(); i++) {
				if (i != 0)
					filesStr.append("\n");
				filesStr.append(files[i]);
			}	
			
			files.clear();
			return filesStr;
		}
		
		int GetFilesInDir (string dir, vector<string> &files) {
			DIR *dp;
			struct dirent *dirp;
			
			// try to open directory provided
			if((dp  = opendir(dir.c_str())) == NULL) {
				if (DEBUG) cout << "Problem opening " << dir << endl;
				return -1;
			}

			// copy file names into vector
			while ((dirp = readdir(dp)) != NULL) {
				files.push_back(string(dirp->d_name));
			}
			
			closedir(dp);
			return 0;
		}
};


//----------------------------------------------------
// THREAD HANDLER (PASSIVE CLIENT) 
//----------------------------------------------------

void* StartListeningThread(void* p) {
	Client* client = (Client*)p;
	client->RegisterWithServer();
	client->ListenForRequests();
}

//----------------------------------------------------
// AUTORUN 
//----------------------------------------------------

int main(int argc, char *argv[]) {

	char* bootstrap = (char*) malloc(100);
	int bootstrapPort;

	int myPassivePort;		// request handling
	int myActivePort;		// user controlled, request sender

	/* Test values *
	bootstrap = "uw1-320-19.uwb.edu";
	*/

	// main server host
	cout << "Main server hostname: ";
	cin >> bootstrap;

	// main server port
	cout << "Main server port (0 for default): ";
	cin >> bootstrapPort;
	if (bootstrapPort == 0)
		bootstrapPort = 7348;

	// our port
	cout << "Port to run client (0 for default): ";
	cin >> myPassivePort;
	if (myPassivePort == 0)
		myPassivePort = 7348;

	// active port is just passive port + 1
	myActivePort = myPassivePort + 1;

	// initialize active and passive clients
	Client listenClient(myPassivePort, bootstrap, bootstrapPort);
	Client commandClient(myActivePort, bootstrap, bootstrapPort);

	// passive should know about active and vice versa
	listenClient.SetPartnerPort(myActivePort);
	commandClient.SetPartnerPort(myPassivePort);

	// create seperate thread for passive listening client
    pthread_t listeningThread;
	pthread_create(&listeningThread, NULL, &StartListeningThread, &listenClient);



	// Start the Main Program  --------------------------------------------------------


	int input = -1;
	string filename = "";

	cout << "------------------------------------------------------------" << endl;
	cout << "-------------------------- P2P -----------------------------" << endl;
	cout << "------------------- CSS 432 - Fall 2013 --------------------" << endl;
	cout << "-------------------- by Andrew Harvey ----------------------" << endl;
	cout << "------------------------------------------------------------" << endl << endl;

	sleep(1);	// give passive thread a second to connect


	while (input != 0) {
	
		// print menu
		cout << endl;
		cout << "1: Show peers" << endl;
		cout << "2: Show available files" << endl;
		cout << "3: Search for a file" << endl;
		cout << "4: Download a file" << endl;
		cout << "0: Exit" << endl;
		cout << endl;
		
		// get input
		cout << "Enter menu number: ";
		cin >> input;
		cout << endl;
		
		// handle input
		switch (input) {
			case 0:
				break;
				
			case 1:
				commandClient.PrintPeers();
				break;
				
			case 2:
				commandClient.ListPeerFiles();
				break;
				
			case 3:
				cout << "Filename: ";
				cin >> filename;
				commandClient.SearchForFile(filename);
				break;
				
			case 4:
				cout << "Filename: ";
				cin >> filename;
				commandClient.RequestFile(filename);
				break;

			default:
				cout << "Invalid selection!" << endl;
				break;
		}
	}

	cout << "Exiting..." << endl;
	listenClient.UnregisterFromServer();
	pthread_exit(NULL);
	exit(0);
	
}

